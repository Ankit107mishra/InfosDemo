package com.ankit.infosdemo.adpter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.ankit.infosdemo.ItemViews.ListItems;
import com.ankit.infosdemo.R;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{

    ArrayList<ListItems> itemsArrayList;
    Activity activity;
    AQuery aQuery;

    public ListAdapter(ArrayList<ListItems> itemsArrayList, Context activity) {
        this.itemsArrayList = itemsArrayList;
        this.activity = (Activity) activity;
        aQuery = new AQuery(activity);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ListItems item = (ListItems)itemsArrayList.get(position);
        holder.title.setText(item.getTitle());
        holder.description.setText(item.getDescription());
        if(item.getDescription().equals("")){
            holder.description.setVisibility(View.GONE);
        }
        aQuery.id(holder.image).progress(holder.progress).image(item.getImageHref(),true,true);

        /*if(item.getImageHref().equals(""))
            holder.rl_image.setVisibility(View.GONE);*/

    }

    @Override
    public int getItemCount() {
        return itemsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ImageView image;
        public ImageView img_arrow;
        public TextView title;
        public TextView description;
        public ProgressBar progress;
        public RelativeLayout rl_image;

        private void findViews(View root) {
            image = (ImageView)root.findViewById( R.id.image );
            img_arrow = (ImageView)root.findViewById( R.id.img_arrow );
            title = (TextView) root.findViewById( R.id.title );
            description = (TextView) root.findViewById( R.id.description );
            progress = (ProgressBar)root.findViewById( R.id.progress );
            rl_image = (RelativeLayout)root.findViewById( R.id.rl_image );
        }

        public ViewHolder(View view) {
            super(view);
            mView = view;
            findViews(mView);

        }

    }

}
