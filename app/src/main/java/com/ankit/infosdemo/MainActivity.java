package com.ankit.infosdemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.ankit.infosdemo.Common.Constant;
import com.ankit.infosdemo.ItemViews.ListItems;
import com.ankit.infosdemo.Utils.VolleyResponseListener;
import com.ankit.infosdemo.Utils.VolleyUtils;
import com.ankit.infosdemo.adpter.ListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<ListItems> itemsArrayList;
    ProgressDialog dialog;
    ListAdapter adapter;
    ListItems listItems;
    TextView toolbar_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);

        sendRequest();
    }

    private void sendRequest() {

        dialog = new ProgressDialog(this);

        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String SERVER_URL = Constant.URL;
        Log.e("Server_url"," url  "+SERVER_URL);

        sendServerRequest(this,SERVER_URL, VolleyResponseListener.JSON_FACTS);
    }

    private void sendServerRequest(final Context ctx, final String url, final int responseId) {
        VolleyUtils.makeJsonGETReq(Request.Method.GET, ctx, url, responseId, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                dialog.dismiss();
            }

            @Override
            public void onResponse(Object response, int responseId) {
                dialog.dismiss();
                if (responseId == VolleyResponseListener.JSON_FACTS) {
                    Log.e("Response "," response  "+response.toString());
                    itemsArrayList = new ArrayList<ListItems>();
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        toolbar_title.setText(jsonObject.getString("title"));
                        JSONArray jsonArray = jsonObject.getJSONArray("rows");
                        for(int i=0;i<jsonArray.length();i++)
                        {
                            String title = jsonArray.getJSONObject(i).getString("title").equals("null")?"":jsonArray.getJSONObject(i).getString("title");
                            String description = jsonArray.getJSONObject(i).getString("description").equals("null")?"":jsonArray.getJSONObject(i).getString("description");
                            String image = jsonArray.getJSONObject(i).getString("imageHref").equals("null")?"":jsonArray.getJSONObject(i).getString("imageHref");
                            if(title.equals("") && description.equals("") && image.equals("") ){

                            }else{
                                listItems = new ListItems(title,description,image);
                                itemsArrayList.add(listItems);
                            }

                            if(itemsArrayList.size()>0){
                                recyclerView.setAdapter(new ListAdapter(itemsArrayList,MainActivity.this));
                            }else {
                                Toast.makeText(ctx, "No Items Found", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                }
            }
        });
    }

}


