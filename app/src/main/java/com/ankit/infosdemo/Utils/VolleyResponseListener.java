package com.ankit.infosdemo.Utils;

public interface VolleyResponseListener {

    public static int JSON_FACTS = 195;

    void onError(String message);

    void onResponse(Object response, final int responseId);

}
